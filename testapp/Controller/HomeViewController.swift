//
//  HomeViewController.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import UIKit



class HomeViewController: ViewController, UsersScreenDelegate {
    
    // MARK: UsersScreenDelegate Implementation Function
    func backFromUsersScreen() {
        setUserSelectedData()
        //self.homeViewLoaded.centerNameLabel.text =
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var centerView: UIView!
        
    let homeViewLoaded = HomeComponents.loadNib()
    var nameInput: String = "Anonymous"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Home"
        homeViewLoaded.delegate = self
        homeViewLoaded.frame = centerView.frame
        scrollView.contentSize.height = homeViewLoaded.contentHomeView.frame.height
        centerView.addSubview(homeViewLoaded)
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        
        
        DispatchQueue.main.async {
            self.setUserSelectedData()
            self.homeViewLoaded.topNameLabel.text = self.nameInput
        }
    }
    
    @objc func rotated() {
        /// to support scrollview when changing orientation
        scrollView.contentSize.height = homeViewLoaded.contentHomeView.frame.height
    }
    
    func setUserSelectedData() {
        if let namePref = Preferences.sharedInstance.readPreference(Preferences.keyNameUser) as? String, let emailPref = Preferences.sharedInstance.readPreference(Preferences.keyEmailuser) as? String, let imagePref = Preferences.sharedInstance.readPreference(Preferences.keyImageUser) as? String {
            
            DispatchQueue.main.async {
                self.homeViewLoaded.centerNameLabel.isHidden = false
                self.homeViewLoaded.centerEmailLabel.isHidden = false
                self.homeViewLoaded.centerWebButton.isHidden = false
                self.homeViewLoaded.centerSelectLabel.isHidden = true
                
                self.homeViewLoaded.centerNameLabel.text = namePref
                self.homeViewLoaded.centerEmailLabel.text = emailPref
                self.homeViewLoaded.profileImageView.downloaded(from: imagePref)
            }
            
        } else {
            print("nodataselected")
            self.homeViewLoaded.centerNameLabel.isHidden = true
            self.homeViewLoaded.centerEmailLabel.isHidden = true
            self.homeViewLoaded.centerWebButton.isHidden = true
            self.homeViewLoaded.centerSelectLabel.isHidden = false
        }
    }
    
}

extension HomeViewController: HomeComponentsDelegate {
    func onClickChooseButton() {
        if let usersVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "usersScreen") as? UsersViewController {
            usersVC.delegate = self
            self.navigationController?.pushViewController(usersVC, animated: true)
        }
    }
    
    func onClickWebButton() {
        print("onClickWebButtonPressed")
        self.navigationController?.pushViewController(WebViewController(), animated: true)
    }
    
    
}
