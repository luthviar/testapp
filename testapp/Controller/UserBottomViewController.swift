//
//  UserBottomViewController.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import UIKit

class UserBottomViewController: UIViewController {

    var userData: User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        let bottomView = BottomView.loadNib()
        bottomView.frame = self.view.frame
        bottomView.delegate = self
        if let user = userData {
            bottomView.imageView.downloaded(from: user.avatar)
            bottomView.fullnameLabel.text = "\(user.first_name) \(user.last_name)"
            
        }
        self.view.addSubview(bottomView)
        // Do any additional setup after loading the view.
    }
    
}

extension UserBottomViewController: BottomViewDelegate {
    func onClickSelectButton() {
        if let currentUserData = userData {
            let nameUser: String = "\(currentUserData.first_name) \(currentUserData.last_name)"
            Preferences.sharedInstance.writePreference(Preferences.keyNameUser, value: nameUser as AnyObject)
            Preferences.sharedInstance.writePreference(Preferences.keyEmailuser, value: currentUserData.email as AnyObject)
            Preferences.sharedInstance.writePreference(Preferences.keyImageUser, value: currentUserData.avatar as AnyObject)
        }
        dismissCurrentScreen()
    }
    
    func dismissCurrentScreen() {
        self.dismiss(animated: true) {
            AppGlobal.usersVC?.delegate?.backFromUsersScreen()
            AppGlobal.usersVC?.navigationController?.popViewController(animated: true)
        }
    }
}
