//
//  UsersViewController.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import UIKit
import MapKit

protocol UsersScreenDelegate {
    func backFromUsersScreen()
}

class UsersViewController: UIViewController {
              
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    var usersData: [User] = []
    var currentResponseInfo: ResponseInformation? = nil
    let refreshControl = UIRefreshControl()
    var delegate: UsersScreenDelegate? = nil
    var isTableViewActive: Bool = true
    
    let mapView : MKMapView = {
        let map = MKMapView()
        map.overrideUserInterfaceStyle = .unspecified
        return map
    }()
    
    let indexToCoordinate: [Int: CLLocationCoordinate2D] = [:]
    
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        AppGlobal.usersVC = self
        self.title = "Users"
        let buttonPin = UIBarButtonItem(image: UIImage(named: "pinpointicon"), style: .plain, target: self, action: #selector(actionBarPressed))
        self.navigationItem.rightBarButtonItem  = buttonPin
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        
        isTableViewActive ? setupTableView() : setupMapView()
    }
    
    
    
    @objc func refresh(_ sender: AnyObject) {
        // Code to refresh table view
        loadFirstData()
        refreshControl.endRefreshing()
    }
    
    @objc func actionBarPressed() {
        print("actionBarPressedhere")
        if isTableViewActive {
            /// action: mengubah menjadi mapview dan bar item menjadi list view
            let buttonList = UIBarButtonItem(image: UIImage(named: "ic_show_list"), style: .plain, target: self, action: #selector(actionBarPressed))
            self.navigationItem.rightBarButtonItem  = buttonList
            self.tableView.removeFromSuperview()
            self.setupMapView()
        } else {
            /// action: mengubah menjadi tableview dan bar item menjadi map view
            let buttonPin = UIBarButtonItem(image: UIImage(named: "pinpointicon"), style: .plain, target: self, action: #selector(actionBarPressed))
            self.navigationItem.rightBarButtonItem  = buttonPin
            self.mapView.removeFromSuperview()
            self.setupTableView()
        }
        isTableViewActive = !isTableViewActive
    }
    
    func loadFirstData() {
        AppClient.getUsers { isConnected, dataResponse, responseInfo, error in
            guard let userDataResponse = dataResponse else { return }
            guard let responseInfo = responseInfo else { return }
            guard isConnected == .connected else { return }
            guard error == nil else { return }
            self.usersData = userDataResponse
            self.currentResponseInfo = responseInfo
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func loadMoreData() {
        guard let currentResponseInfo = currentResponseInfo else {
            return
        }

        guard currentResponseInfo.page != currentResponseInfo.total_pages else {
            return
        }
        
        AppClient.getUsers(currentInfo: currentResponseInfo) { isConnected, dataResponse, responseInfo, error in
            guard let userDataResponse = dataResponse else { return }
            guard let responseInfo = responseInfo else { return }
            guard isConnected == .connected else { return }
            guard error == nil else { return }
            self.usersData += userDataResponse
            self.currentResponseInfo = responseInfo
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.register(UsersTableViewCell.self, forCellReuseIdentifier: "UsersTableViewCell")
        tableView.register(UINib(nibName: "UsersTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersTableViewCell")
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        loadFirstData()
    }
    
    func generateCustomDataLatLong() {
        let latLongDummyArray: [[String:Double]] = [
            ["lat":-6.160550,"long":106.824225],
            ["lat":-6.149286,"long":106.850679],
            ["lat":-6.143824,"long":106.876102],
            ["lat":-6.187515,"long":106.896371],
            ["lat":-6.191611,"long":106.866482],
            ["lat":-6.196731,"long":106.831096]
        ]
        AppClient.getUsers { isConnected, dataResponse, responseInfo, error in
            guard let userDataResponse = dataResponse else { return }
            guard let responseInfo = responseInfo else { return }
            guard isConnected == .connected else { return }
            guard error == nil else { return }
            self.usersData = userDataResponse
            self.currentResponseInfo = responseInfo
            if self.usersData.count <= latLongDummyArray.count {
                for (index,dummy) in latLongDummyArray.enumerated() {
                    self.usersData[index].latitude = dummy["lat"]
                    self.usersData[index].longitude = dummy["long"]
                }
                self.addPins()
            }
        }
    }
    
    
    func setupMapView() {
        
        if Connectivity.isConnectedToNetwork() == false {
            self.present(showAlert(message: "Please Connect to Internet."), animated: true)
            return
        }
        
        mapView.delegate = self
        view.addSubview(mapView)
               
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        generateCustomDataLatLong()
    }
    
    func addPins() {
        var lastPin = CustomAnnotation()
        for (index,user) in usersData.enumerated() {
            let thePin = CustomAnnotation()
            thePin.title = user.first_name
            thePin.user = user
            thePin.coordinate = CLLocationCoordinate2D(
                latitude: user.latitude ?? 0,
                longitude: user.longitude ?? 0
            )
            mapView.addAnnotation(thePin)
            lastPin = thePin
        }
        let region = MKCoordinateRegion(center: lastPin.coordinate, latitudinalMeters: 15000, longitudinalMeters: 15000)
        mapView.setRegion(region, animated: true)
    }
    
    
    // MARK: Helper Function
    func showAlert(message: String) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alert
    }
}

extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if usersData.count > 0 {
            return usersData.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell", for: indexPath) as! UsersTableViewCell
        
        
        DispatchQueue.main.async {
            if self.usersData.count > 0 {
                let currentUserData = self.usersData[indexPath.row]
                cell.nameUser.text = "\(currentUserData.first_name) \(currentUserData.last_name)"
                cell.emailUser.text = currentUserData.email
                cell.imageUser.downloaded(from: currentUserData.avatar)
            } else {
                cell.nameUser.text = "No Data"
                cell.emailUser.text = "Please reload"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selectedrowat: \(indexPath)")
        tableView.deselectRow(at: indexPath, animated: true)
        let currentUserData: User = usersData[indexPath.row]
        let nameUser: String = "\(currentUserData.first_name) \(currentUserData.last_name)"
        Preferences.sharedInstance.writePreference(Preferences.keyNameUser, value: nameUser as AnyObject)
        Preferences.sharedInstance.writePreference(Preferences.keyEmailuser, value: currentUserData.email as AnyObject)
        Preferences.sharedInstance.writePreference(Preferences.keyImageUser, value: currentUserData.avatar as AnyObject)
        self.delegate?.backFromUsersScreen()
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if usersData.count > 0 {
            let lastElement = usersData.count - 1
            if indexPath.row == lastElement {
                loadMoreData()
            }
        }
    }
}

extension UsersViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let customAnnotation: CustomAnnotation = view.annotation as? CustomAnnotation {
            let region = MKCoordinateRegion(center: customAnnotation.coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
            mapView.setRegion(region, animated: true)
//            Preferences.sharedInstance.writePreference(Preferences.keyNameUser, value: customAnnotation.fullname as AnyObject)
//            Preferences.sharedInstance.writePreference(Preferences.keyImageUser, value: customAnnotation.avatar as AnyObject)
//            Preferences.sharedInstance.writePreference(Preferences.keyEmailuser, value: customAnnotation.email as AnyObject)
            if let userData: User = customAnnotation.user {
                presentBottomSheet(user: userData)
            }
        }
    }
    
    func presentBottomSheet(user: User? = nil) {
        let userBottomVC = UserBottomViewController()
        userBottomVC.userData = user
        
        let nav = UINavigationController(rootViewController: userBottomVC)
        // 1
        nav.modalPresentationStyle = .pageSheet

        
        // 2
        if let sheet = nav.sheetPresentationController {

            // 3
            sheet.detents = [.medium()]

        }
        
        let close = UIBarButtonItem(title: "Close", primaryAction: .init(handler: { _ in
            self.dismiss(animated: true)
        }))
        userBottomVC.navigationItem.rightBarButtonItem = close
        // 4
        present(nav, animated: true, completion: nil)

    }
}

class CustomAnnotation : MKPointAnnotation {
    var user: User?
}
