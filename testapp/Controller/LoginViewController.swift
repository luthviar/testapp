//
//  LoginViewController.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import UIKit

class LoginViewController: ViewController  {
        
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let loginViewLoaded = LoginComponents.loadNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginViewLoaded.delegate = self
        loginViewLoaded.frame = centerView.frame
        centerView.addSubview(loginViewLoaded)
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.contentSize.height = loginViewLoaded.contentLoginView.frame.height
    }
    
    @objc func rotated() {
        /// to support scrollview when changing orientation
        scrollView.contentSize.height = loginViewLoaded.contentLoginView.frame.height
    }
        
    
    func isPalindrome(word: String) -> Bool {
        return word == String(word.reversed())
    }
    
    func showAlert(message: String) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alert
    }
}

extension LoginViewController: LoginComponentsDelegate {
    
    func onClickCheckButton() {
        let isPalindrome: Bool = isPalindrome(word: loginViewLoaded.palindromeTextField.text ?? "")
        if loginViewLoaded.palindromeTextField.text != "" {
            self.present(showAlert(message: isPalindrome ? "isPalindrome" : "not palindrome"), animated: true)
        } else {
            self.present(showAlert(message: "please fill the Palindrome text field"), animated: true)
        }
    }
    
    func onClickNextButton() {
        if let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeScreen") as? HomeViewController {
            let nameTextFieldInput: String = (loginViewLoaded.nameTextField.text ?? "Anonymous") == "" ? "Anonymous" : loginViewLoaded.nameTextField.text!
            homeVC.nameInput = nameTextFieldInput
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
}
