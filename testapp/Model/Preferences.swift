//
//  Preferences.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import Foundation
import UIKit


class Preferences: NSObject {
    @objc static let sharedInstance: Preferences = Preferences()
    fileprivate let preferences: UserDefaults
    
    static let keyNameUser: String = "name_user"
    static let keyEmailuser: String = "email_user"
    static let keyImageUser: String = "image_user"
    
    override init() {
        self.preferences = UserDefaults.standard
    }
    
    @objc func readPreference(_ key: String) -> AnyObject? {
        return self.preferences.value(forKey: key) as AnyObject?
    }
    
    @objc func writePreference(_ key: String, value: AnyObject?) {
        self.preferences.setValue(value, forKey: key)
        
        let didSaved: Bool = self.preferences.synchronize()
        
        if didSaved == false {
            print("Couldn't save preferences")
        }
    }
}
