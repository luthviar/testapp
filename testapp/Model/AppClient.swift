//
//  AppClient.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import Foundation
import SystemConfiguration

class AppClient {
    
    
    
    static func getUsers(currentInfo: ResponseInformation? = nil, completionHandler: @escaping (Connectivity.Status, [User]?, ResponseInformation?, Error?) -> Void) {
        
        var baseURLString: String = "https://reqres.in/api/users?page=1&per_page=6"
        if let currentInfo = currentInfo {
            baseURLString = "https://reqres.in/api/users?page=\(currentInfo.page+1)&per_page=6"
        }
        AppClient.taskForRequest(url: URL(string: baseURLString)!, method: "GET", body: "") { dataResponse, error in
            guard let dataResponse = dataResponse else { completionHandler(.connected, nil, nil, error); return }
            guard let _ = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as? [String: Any] else {
                completionHandler(.connected, nil, nil, nil)
                return
            }
                        
            if let jsonString = String(data: dataResponse, encoding: .utf8) {
                let theDictionary = AppClient.convertToDictionary(text: jsonString)
                let responseInfo = ResponseInformation(page: theDictionary!["page"] as! Int, per_page: theDictionary!["per_page"] as! Int, total: theDictionary!["total"] as! Int, total_pages: theDictionary!["total_pages"] as! Int)
                do {
                    let decoder = JSONDecoder()
                    let usersDict: [[String:Any]] = theDictionary!["data"] as! [[String:Any]]
                    let jsonData = try JSONSerialization.data(withJSONObject: usersDict, options: .prettyPrinted)
                    let users = try decoder.decode([User].self, from: jsonData)
                    completionHandler(.connected, users, responseInfo, nil)
                } catch {
                    print(error)
                    completionHandler(.connected, nil, nil, nil)
                }
            } else {
                completionHandler(.connected, nil, nil, nil)
            }
        }
    }        
    
    // MARK: Universal function helper to call for request API URL
    class func taskForRequest(
        url: URL,
        method: String,
        body: String,
        completion: @escaping(Data?, Error?) -> Void
    ) {
        guard Connectivity.isConnectedToInternet == true else {
            completion(nil, AppError.networkError)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
        
            let responseObject = data
            DispatchQueue.main.async {
                completion(responseObject, nil)
            }
       
        }
        task.resume()
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}

public enum AppError: Error {
    case networkError
}

extension AppError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .networkError:
            return NSLocalizedString("No network is connected.", comment: "")
        }
    }
}

// MARK: Universal class helper to check internet connection
class Connectivity {
    static var isConnectedToInternet: Bool {
        return isConnectedToNetwork()
    }
    
    enum Status {
        case notConnected, connected, other
    }
        
    class func isConnectedToNetwork() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
           $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
               SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
           }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
           return false
        }

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret
    }
}

