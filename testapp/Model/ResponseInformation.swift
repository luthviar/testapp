//
//  ResponseInformation.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import Foundation

struct ResponseInformation: Codable {
    let page: Int
    let per_page: Int
    let total: Int
    let total_pages: Int
}
