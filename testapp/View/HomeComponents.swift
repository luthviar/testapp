//
//  HomeComponents.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import UIKit

protocol HomeComponentsDelegate {
    func onClickChooseButton()
    func onClickWebButton()
}


class HomeComponents: UIView {

    @IBOutlet weak var topNameLabel: UILabel!
    @IBOutlet weak var contentHomeView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var centerSelectLabel: UILabel!
    @IBOutlet weak var centerNameLabel: UILabel!
    @IBOutlet weak var centerEmailLabel: UILabel!
    @IBOutlet weak var centerWebButton: UIButton!
    @IBOutlet weak var chooseButton: UIButton!
    
    var delegate: HomeComponentsDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print("awakeFromNibHomeComponents")
        chooseButton.tag = 1
        centerWebButton.tag = 2
    }
    
    @IBAction func pressedButton(_ sender: UIButton) {
        if sender.tag == 1 {
            delegate?.onClickChooseButton()
        } else if sender.tag == 2 {
            delegate?.onClickWebButton()
        }
    }
}
