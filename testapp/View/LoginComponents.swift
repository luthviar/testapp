//
//  LoginComponents.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import UIKit

protocol LoginComponentsDelegate {
    func onClickCheckButton()
    func onClickNextButton()
}

class LoginComponents: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var palindromeTextField: UITextField!
    @IBOutlet weak var checkButton: UIButton!        
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var contentLoginView: UIView!
    var delegate: LoginComponentsDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print("awakeFromNibLoginComponents")
        checkButton.tag = 1
        nextButton.tag = 2
    }
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            delegate?.onClickCheckButton()
        } else if sender.tag == 2 {
            delegate?.onClickNextButton()
        }
    }
    
}
