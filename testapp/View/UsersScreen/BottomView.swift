//
//  BottomView.swift
//  testapp
//
//  Created by Luthfi Abdurrahim on 23/05/22.
//

import Foundation
import UIKit

protocol BottomViewDelegate {
    func onClickSelectButton()
}

class BottomView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    var delegate: BottomViewDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
    }
    @IBAction func pressedButton(_ sender: UIButton) {
        delegate?.onClickSelectButton()
    }
}
